/**
 * 
 */
package org.neill.workouttracker.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;


/**
 * @author neill
 *
 */
@Entity
public class Workout {
	
	@Id
	@Column(name = "workout_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	private String workoutName;
	private Integer sets;
	private Integer reps;
	private Integer weight;
	@ManyToOne
	@JoinColumn(name="user_id")
	private User user;

	/**
	 * 
	 */
	public Workout() {
		// TODO Auto-generated constructor stub
	}
	
	public Workout(String workoutName, Integer sets, Integer reps, Integer weight, User user) {
		this.workoutName = workoutName;
		this.sets = sets;
		this.reps = reps;
		this.weight = weight;
		this.user = user;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getWorkoutName() {
		return workoutName;
	}

	public void setWorkoutName(String workoutName) {
		this.workoutName = workoutName;
	}

	public Integer getSets() {
		return sets;
	}

	public void setSets(Integer sets) {
		this.sets = sets;
	}

	public Integer getReps() {
		return reps;
	}

	public void setReps(Integer reps) {
		this.reps = reps;
	}

	public Integer getWeight() {
		return weight;
	}

	public void setWeight(Integer weight) {
		this.weight = weight;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	
	
}
