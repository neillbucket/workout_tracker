/**
 * 
 */
package org.neill.workouttracker.rest;

import java.util.Optional;

import org.neill.workouttracker.model.Workout;
import org.neill.workouttracker.service.WorkoutService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author neill
 *
 */
@RestController
@RequestMapping("/Workout")
public class WorkoutController {
	
	@Autowired
	private WorkoutService workoutService;
	
	@GetMapping("/all")
	public Iterable<Workout> getAllWorkouts(){
		return workoutService.getAllWorkouts();
	}
	
	@GetMapping("/{id}")
	public Optional<Workout> findWorkoutById(@PathVariable Integer id) {
		return workoutService.findWorkoutById(id);
	}
	
	@PostMapping("/add")
	@ResponseBody
	public boolean addWorkout(@RequestBody Workout workout) {
		workoutService.addWorkout(workout);
		return true;
	}

}
