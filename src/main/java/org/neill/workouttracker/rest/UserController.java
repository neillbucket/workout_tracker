/**
 * 
 */
package org.neill.workouttracker.rest;

import org.neill.workouttracker.model.User;
import org.neill.workouttracker.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author neill
 *
 */
@RestController
@RequestMapping("/Login")
public class UserController {
	
	@Autowired
	private UserService userService;
	
	@GetMapping("/all")
	public Iterable<User> getAllUsers() {
		return userService.getAllUsers();
	}
	
	@PostMapping("/add")
	public boolean addUser(@RequestBody User user) {
			
		return userService.addUser(user);
	}

}
