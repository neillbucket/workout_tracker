/**
 * 
 */
package org.neill.workouttracker.service;

import org.neill.workouttracker.dao.UserRepository;
import org.neill.workouttracker.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;



/**
 * @author neill
 *
 */
@Component
public class UserService {

	/**
	 * 
	 */
	public UserService() {
		// TODO Auto-generated constructor stub
	}
	
	@Autowired
	private UserRepository userRepository;
	
	public Boolean addUser(User user) {
		userRepository.save(user);
		return true;
	}
	
	public Iterable<User> getAllUsers() {
		return userRepository.findAll();
	}
	

}
