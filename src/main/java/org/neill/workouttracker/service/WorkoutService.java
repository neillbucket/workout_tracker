/**
 * 
 */
package org.neill.workouttracker.service;

import java.util.Optional;

import org.neill.workouttracker.dao.WorkoutRepository;
import org.neill.workouttracker.model.Workout;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author neill
 *
 */
@Component
public class WorkoutService {
	
	@Autowired
	private WorkoutRepository workoutRepository;
	
	/**
	 * 
	 */
	public WorkoutService() {
		// TODO Auto-generated constructor stub
	}
	
	public Iterable<Workout> getAllWorkouts(){
		return workoutRepository.findAll();
	}
	
	public Optional<Workout> findWorkoutById(Integer id) {
		return workoutRepository.findById(id);
	}

	public boolean addWorkout(Workout workout) {
		workoutRepository.save(workout);
		return true;
	}

}
