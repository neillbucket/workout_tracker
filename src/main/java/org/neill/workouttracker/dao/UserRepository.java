/**
 * 
 */
package org.neill.workouttracker.dao;

import org.neill.workouttracker.model.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * @author neill
 *
 */
@Repository
public interface UserRepository extends CrudRepository<User, Integer>{
    @Query("select new org.neill.workouttracker.model.User(u.id, u.name, u.password) from User u where u.name = ?1 and u.password = ?2")
    User findByNameAndPassword(String name, String password);
}

