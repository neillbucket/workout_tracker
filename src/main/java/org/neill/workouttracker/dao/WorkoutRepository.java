/**
 * 
 */
package org.neill.workouttracker.dao;

import org.neill.workouttracker.model.Workout;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * @author neill
 *
 */
@Repository
public interface WorkoutRepository extends CrudRepository<Workout, Integer> {}

